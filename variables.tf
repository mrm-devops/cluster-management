variable "region" {
  default     = "us-east-2"
  description = "AWS region"
}

variable "cluster_name" {
  default     = "gitlab-terraform-eks"
  description = "EKS Cluster name"
}

variable "domain_address" {
  description = "Domain address (domain with wildcard registred in route53)"
}