resource "kubernetes_manifest" "traefik_dashboard" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"

    metadata = {
      name      = "dashboard"
      namespace = "gitlab-managed-traefik"
    }

    spec = {
      entryPoints = ["web", "websecure"]

      routes = [{
        match = "Host(`traefik.${var.domain_address}`) && (PathPrefix(`/dashboard`) || PathPrefix(`/api`))"
        kind  = "Rule"
        services = [{
          name = "api@internal"
          kind = "TraefikService"
        }]
      }]
    }
  }
}

resource "kubernetes_manifest" "grafana_dashboard" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"

    metadata = {
      name      = "grafana-dashboard"
      namespace = "gitlab-managed-monitoring"
    }

    spec = {
      entryPoints = ["web", "websecure"]

      routes = [{
        match = "Host(`grafana.${var.domain_address}`) && PathPrefix(`/`)"
        kind  = "Rule"
        services = [{
          name      = "prometheus-stack-grafana"
          kind      = "Service"
          namespace = "gitlab-managed-monitoring"
          port      = 80
        }]
      }]
    }
  }
}

resource "kubernetes_manifest" "argocd_dashboard" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"

    metadata = {
      name      = "argocd-dashboard"
      namespace = "gitlab-managed-argocd"
    }

    spec = {
      entryPoints = ["web", "websecure"]

      routes = [
        {
          match    = "Host(`argocd.${var.domain_address}`)"
          priority = 10
          kind     = "Rule"
          services = [{
            name = "argocd-server"
            port = 80
          }]
        },
        {
          match    = "Host(`argocd.${var.domain_address}`) && Headers(`Content-Type`, `application/grpc`)"
          priority = 11
          kind     = "Rule"
          services = [{
            name   = "argocd-server"
            port   = 80
            scheme = "h2c"
          }]
        }
      ]
    }
  }
}
